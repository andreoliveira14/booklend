<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the languages seed.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Delete all the languages
         */
        DB::table('languages')->delete();

        /*
         * Insert languages
         */
        DB::table('languages')->insert([
            'name'     => 'Portuguese',
            'initials' => 'PT'
        ]);

        DB::table('languages')->insert([
            'name'     => 'English',
            'initials' => 'PT'
        ]);

        DB::table('languages')->insert([
            'name'     => 'Spanish',
            'initials' => 'ES'
        ]);

        DB::table('languages')->insert([
            'name'     => 'French',
            'initials' => 'FR'
        ]);

        /*
         * Info
         */
        $this->command->info('Language table seeded!');
    }
}
