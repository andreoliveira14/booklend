# Booklend - Laravel Project

This project is a web site where you can add books to your shelf and then you can lend them to another person.

### SETUP
###### Install PHP (you can have a look on 'docs' folder of this project)
###### Install Composer
###### Install Node.js
###### Once you have the project on your machine, open it in a console and make 'composer install'
###### Then, make 'npm install'
###### Then, make 'gulp'
###### Now, just copy the '.env.example' to '.env' file and copy the database info from
###### config/database.php. CHANGE THE DB_HOST TO YOUR SQL HOST (you can see it on SQL SERVER MANAGEMENT just before login)
###### And you are ready to go: 'php artisan serve'

### DATABASE SETUP
If you are in other computer: go to SQL Server Management, Security, and properties of
'sa' user. Go to status and check 'Enabled'. Then, go to General tab and put the password
there is on .env or config/database.php.
Go to SQL Server Manager, and on properties of your connection:
Security - check the SQL Server and Windows  Authen....

### TOOLS
###### ATOM and Packages (you can have a look on the packages I have installed on 'docs' folder of this project)
###### SQL SERVER 2014
###### Git Extensions
###### Git Bash
###### ConEmu

# INSTALL NEW PACKAGES IN THE PROJECT (like jquery, etc)
###### npm

# Common Errors:

##### "RuntimeException: No supported encrypter found. The cipher and / or key length are invalid."
Create a .env file and then run 'php artisan key:generate'
