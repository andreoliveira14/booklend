@extends('app.shared.layouts.app')
@section('title', 'Messages')

@section('content')

    {{-- Page Title --}}
    @include('app.shared.layouts._title', ['title' => 'Messages'])

    <div class="row">
        <div class="col-md-12">
            <messages auth-user-id="{{ $authUserId }}" :initial-users="{{ json_encode($users) }}"></messages>
        </div>
    </div>

@endsection