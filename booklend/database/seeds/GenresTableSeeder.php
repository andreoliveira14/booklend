<?php

use Illuminate\Database\Seeder;

class GenresTableSeeder extends Seeder
{
    /**
     * Run the genres seeder.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Delete all the languages
         */
        DB::table('genres')->delete();

        /*
         * Insert genres
         */
        DB::table('genres')->insert([
            'name'     => 'Biography'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Children'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Comics & Graphic Novels'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Computing & Internet'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Crime'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Thrillers'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Mystery'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Fiction'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Food & Drink'
        ]);

        DB::table('genres')->insert([
            'name'     => 'History'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Horror'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Humour'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Romance'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Science Fiction'
        ]);

        DB::table('genres')->insert([
            'name'     => 'Young Adult'
        ]);

        /*
         * Info
         */
        $this->command->info('Genre table seeded!');
    }
}
