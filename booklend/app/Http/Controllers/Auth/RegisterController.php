<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\File;
use Image;
use Request;
use Storage;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/profile';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name'        => 'required|min:2|max:255',
            'email'       => 'required|email|max:255|unique:users',
            'phone'       => 'required|integer',
            'address'     => 'required|min:4|max:255',
            'city'        => 'required|min:2|max:50',
            'country'     => 'required|min:2|max:50',
            'postal_code' => 'required|min:2|max:10',
            'image'       => 'required',
            'password'    => 'required|min:6|confirmed',

        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {
        $user = User::create([
            'name'        => $data['name'],
            'email'       => $data['email'],
            'phone'       => $data['phone'],
            'address'     => $data['address'],
            'city'        => $data['city'],
            'country'     => $data['country'],
            'postal_code' => $data['postal_code'],
            'password'    => bcrypt($data['password']),
        ]);

        //image
        $storagePath = $this->makeImage(Request::file('image'), $user->id);
        $user->image = $storagePath;
        $user->save();

        return $user;
    }

    /**
     * Make the image and save it on public folder.
     *
     * @param array $image
     * @return String
     */
    protected function makeImage($image, $userId) {
        $imageName = $image->getClientOriginalName();
        $imageRealPath = $image->getRealPath();
        $storagePath = '/users/'. $userId . '-' . $imageName;

        $path = public_path("images/users");

        if (!file_exists($path)) {
            File::makeDirectory($path, 0777, true);
        } else {
            chmod($path, 0777);
        }

        Image::make($imageRealPath)->save(public_path('images/' . $storagePath));

        return $storagePath;
    }
}
