
/*
|
|----------------------------------------------------
| MAIN FILE
|----------------------------------------------------
|
| This file is the main JS file. It requires the bootstrap file and all the other js files.
|
|*

/**
 * First we will load all of this project's JavaScript dependencies which
 * include Vue and Vue Resource. This gives a great starting point for
 * building robust, powerful web applications using Vue and Laravel.
 */
require('./bootstrap');
require('./_components');

var app = new Vue({
    el: '#app',
});
