<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBooksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('books', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('author');
            $table->text('summary');
            $table->integer('pages');
            $table->bigInteger('isbn');
            $table->integer('year');
            $table->string('publisher');
            // language
            $table->integer('language_id')->unsigned();
            $table->foreign('language_id')->references('id')->on('languages');
            // type
            $table->integer('format_id')->unsigned();
            $table->foreign('format_id')->references('id')->on('formats');
            // User
            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            // Genre
            $table->integer('genre_id')->unsigned();
            $table->foreign('genre_id')->references('id')->on('genres');
            $table->integer('loan_duration')->nullable();
            $table->boolean('is_deleted')->default(false);
            $table->boolean('to_loan')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('books');
    }
}
