<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;
use Illuminate\Http\UploadedFile;
use Image;

class BookImage extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['path', 'large_path', 'thumb_path', 'ext', 'display_order', 'book_id'];

    /**
     * Product Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function book()
    {
        return $this->belongsTo(Book::class);
    }

    /**
     * Contains uploadedFile instance for creating new image
     */
    protected $file;

    /**
     * Events for BrandImage
     */
    protected static function boot()
    {
        parent::boot();
        static::creating(function (self $image) {

            return $image->setImagePaths();
        });

        static::created(function (self $image) {

            return $image->saveImage();
        });

        static::deleting(function (self $image) {

            foreach (['path', 'thumb_path', 'large_path'] as $path) {
                File::delete(public_path("images/{$image->$path}"));
            }
        });
    }

    /**
     * Static constructor for creating instance with uploaded image
     *
     * @param UploadedFile $file
     * @return static
     */
    public static function createImageFromRequest(UploadedFile $file)
    {
        $image = new static;

        $image->file = $file;
        $image->ext = '.' . $file->getClientOriginalExtension();

        return $image;
    }

    /**
     * Set image paths
     */
    protected function setImagePaths()
    {
        $this->path = "books/medium/";
        $this->large_path = "books/large/";
        $this->thumb_path = "books/thumb/";
    }

    /**
     * Save image after creation
     */
    protected function saveImage()
    {
        $this->checkPaths();

        $this->file = Image::make($this->file);

        //Save actual paths for the image

        $this->path .= "{$this->id}{$this->ext}";
        $this->large_path .= "{$this->id}{$this->ext}";
        $this->thumb_path .= "{$this->id}{$this->ext}";
        $this->save();

        //large image
        $this->file->resize(800, 800, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/' . $this->large_path), 100);

        //Medium (normal) image
        $this->file->resize(300, 300, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/' . $this->path), 100);

        //Thumb Image
        $this->file->resize(170, 170, function ($constraint) {
            $constraint->aspectRatio();
        })->save(public_path('images/' . $this->thumb_path), 100);
    }

    /**
     * Make sure directories exist for images and file permissions are set
     */
    protected function checkPaths()
    {
        foreach (['', '/large', '/medium', '/thumb'] as $dir) {
            $path = public_path("images/books/{$dir}");

            if (!file_exists($path)) {
                File::makeDirectory($path, 0777, true);
            } else {
                chmod($path, 0777);
            }
        }
    }
}
