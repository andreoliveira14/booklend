# Message

* id
* message
* image - one image can be uploaded, e.g, to show the real book.
* userFromId
* userToId
* loanId - if the message is associated to one loan. This column can be null if is a simple message.
* isDeleted
