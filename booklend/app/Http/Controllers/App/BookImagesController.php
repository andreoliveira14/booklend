<?php

namespace App\Http\Controllers\App;

use App\Book;
use App\BookImage;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class BookImagesController extends Controller
{
    /**
     * API - Store a book image.
     *
     * @param  \Illuminate\Http\Request $request
     * @param Book $book
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Book $book)
    {
        $image = BookImage::createImageFromRequest($request->file('file'));
        $image->display_order = $book->images->max('display_order') + 1;

        $book->images()->save($image);
    }

    /**
     * API - Remove a book image.
     *
     * @param  \Illuminate\Http\Request $request
     * @param BookImage $bookImage
     * @return \Illuminate\Http\Response
     * @internal param Book $book
     */
    public function destroy(Request $request, BookImage $bookImage)
    {
        $bookImage->delete();
    }
}
