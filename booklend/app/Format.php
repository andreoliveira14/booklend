<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Format extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'name'
    ];

    /**
     * The rules to validate.
     *
     * @var array
     */
    protected $rules = [
        'name'      => 'required',
    ];
}
