# Instalar o php

## Em windows

Utilizar o web platform installer e escolher os pacotes de PHP:
**Nota**: os pacotes assinalados com (*) são instalados automaticamente.

* PHP 7.0.7 (x64)
* Windows Cache Extension 2.0 (x64) for PHP 7.0
* PHP Manager for IIS (*)

Aceder ao PHP Manager (via IIS) e activar a extensão fileinfo.

### Instalar os drivers de Sql Server

Copiar os ficheiros `php_pdo_sqlsrv_7_nts.dll` e `php_sqlsrv_7_nts.dll`
 deste repositório (ou download [neste endereço](https://github.com/Azure/msphpsql/tree/PHP-7.0/binaries/x64) para a pasta `C:\Program Files\PHP\v7.0\ext`.

> NOTA: Em servidores temos de instalar também um pré-requisito: [Microsoft® ODBC Driver 11 for SQL Server® - Windows](https://www.microsoft.com/en-us/download/details.aspx?id=36434). E tem mesmo de ser o 11 e 64bits, não pode ser o 13 (que também existe)...


Abrir um editor **em modo administrador** e editar o ficheiro `C:\Program Files\PHP\v7.0\php.ini`

Procurar nesse ficheiro a secção `[ExtensionList]`
Apagar todo o conteúdo e colar o seguinte:

```ini
[ExtensionList]
;extension=php_mysql.dll
;extension=php_mysqli.dll
extension=php_mbstring.dll
extension=php_gd2.dll
extension=php_gettext.dll
extension=php_curl.dll
extension=php_exif.dll
extension=php_xmlrpc.dll
extension=php_openssl.dll
extension=php_soap.dll
;extension=php_pdo_mysql.dll
extension=php_pdo_sqlite.dll
extension=php_imap.dll
extension=php_tidy.dll
extension=php_fileinfo.dll
extension=php_pdo_sqlsrv_7_nts.dll
extension=php_sqlsrv_7_nts.dll
```

### Instalar o composer

Gestor de pacotes de php similar ao bundler do ruby.

Fazer o download [do composer](https://getcomposer.org/doc/00-intro.md#installation-windows) e confirmar que o instalador deteta a versão 7 do PHP.

### Configurar a base de dados para aceitar ligações remotas

#### Arrancar o serviço "SQL Server Browser":

- Arrancar o "Computer Management" (procurar por meu computador e 'gerir')
- No painel da esquerda selecionar: _Services and Applications_»_SQL Server Configuration Manager_»_SQL Server Services_
- No painel da direita selecionar "SQL Server Browser"
- Click com o direito e escolher "Properties">"Service"(tab)
- Mudar a opção "Start Mode" para "Automatic"
- OK para fechar.
- Click com o direito e escolher "Start"
