<div class="col-xs-12 col-md-12">
    <div class="alert alert-{{ $type }}" role="alert">{{ $message }}</div>
</div>