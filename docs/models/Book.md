# Book

* id
* title
* author
* summary
* pages
* isbn
* year
* publisher
* image
* language_id - book language
* format_id - type of cover
* user_id - user that owns the book
* genre_id - genre of the book
* is_deleted
