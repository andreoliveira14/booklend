@extends('app.shared.layouts.app')
@section('title', Auth::id() === $user->id ? 'Profile' : 'User - ' . $user->name)

@section('content')

    {{-- Page Title --}}
    @include('app.shared.layouts._title', ['title' => Auth::id() === $user->id ? 'Profile' : $user->name])

    <div class="row">
        <div class="col-md-12">
            <div class="media">
                <div class="media-left">
                    <a href="#">
                        <img src="{{ $user->image ? 'images/' . $user->image : '/assets/images/user-def.jpg' }}" class="thumbnail app-profile-image">
                    </a>
                </div>
                <div class="media-body">
                    <p><small><strong>Phone </strong></small> {{ $user->phone }}</p>
                    <p><small><strong>Email </strong></small> <a href="mailto:{{ $user->email  }}" target="_top">{{ $user->email  }}</a></p>
                    <p><small><strong>Address </strong></small> {{ $user->address }}</p>
                    <p><small><strong>Country </strong></small> {{ $user->country }}</p>
                </div>
            </div>
        </div>
    </div>

    @if(Auth::id() !== $user->id)
        <user-books :user-id="{{ $user->id  }}"></user-books>
    @endif

@endsection