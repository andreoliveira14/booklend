<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'message', 'was_read_user_to', 'user_from_id', 'user_to_id'
    ];

    /**
     * Language Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_from()
    {
        return $this->belongsTo(User::class, 'user_from_id');
    }

    /**
     * Language Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user_to()
    {
        return $this->belongsTo(User::class, 'user_to_id');
    }

}
