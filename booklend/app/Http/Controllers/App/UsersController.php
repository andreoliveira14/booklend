<?php

namespace App\Http\Controllers\App;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

/*
|
|------------------------------------------------------
| UserController
|------------------------------------------------------
|
| UserController has all the actions to interact with user table.
|
 */
class UsersController extends Controller
{
    /**
     * Shows the user details
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        if(is_null($user->id)) {
            $user = Auth::user();
        }

        return view('app.users.show', compact('user'));
    }

    /**
     * Get the user books.
     * Filter the books.
     * @param Request $request
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getBooks(Request $request, User $user)
    {

        //maybe put the initial code in each controller, an then call a Trait that filter the books!


        $user->load(['books' => function ($query) use($request) {
            $query->where('to_loan', true);

            if($request->has('search')) {
                $query->where('title', 'like' , '%' . $request->search . '%');
            }
        }, 'books.images']);

        $books = $user->books;

        return compact('books');
    }
}
