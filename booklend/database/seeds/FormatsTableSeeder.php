<?php

use Illuminate\Database\Seeder;

class FormatsTableSeeder extends Seeder
{
    /**
     * Run the formats seeds.
     *
     * @return void
     */
    public function run()
    {
        /*
         * Delete all the formats
         */
        DB::table('formats')->delete();

        /*
         * Insert formats
         */
        DB::table('formats')->insert([
            'name'     => 'Hadback'
        ]);

        DB::table('formats')->insert([
            'name'     => 'Hardcover'
        ]);

        DB::table('formats')->insert([
            'name'     => 'Paperback'
        ]);

        DB::table('formats')->insert([
            'name'     => 'Audio CD'
        ]);

        DB::table('formats')->insert([
            'name'     => 'Library Binding'
        ]);

        /*
         * Info
         */
        $this->command->info('Formats table seeded!');
    }
}
