<!DOCTYPE html>
<html lang="en">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Booklend - @yield('title')</title>

      <!-- SASS/CSS -->
      <link rel="stylesheet" type="text/css" href="{{ elixir("css/app.css") }}">

    </head>
    <body>
        <nav class="navbar navbar-default">
          <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="{{ url('/') }}">Booklend</a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

              <!-- LEFT-->
              <ul class="nav navbar-nav">
                  @if(!Auth::guest())
                      <li><a href="{{ url('books') }}">My Shelf<span class="sr-only"></span></a></li>
                  @endif
              </ul>

              <!-- SEARCH -->
              <form action="{{ url('search') }}" class="navbar-form navbar-left">
                <div class="form-group">
                  <input type="text" class="form-control" placeholder="Search">
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
              </form>

              <!-- RIGHT-->
              <ul class="nav navbar-nav navbar-right">
                  @if(Auth::guest())
                      <li><a href="{{ url('/login') }}">Login</a></li>
                      <li><a href="{{ url('/register') }}">Register</a></li>
                  @else
                      <li><a href="{{ url('/users/' . Auth::id()) }}">{{ Auth::user()->name }}</a></li>
                      <li><a href="/messages"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span></a></li>
                      <li>
                          <a href="{{ url('/logout') }}"
                             onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                              Logout
                          </a>

                          <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                              {{ csrf_field() }}
                          </form>
                      </li>
                  @endif
              </ul>
            </div><!-- /.navbar-collapse -->
          </div><!-- /.container-fluid -->
        </nav>

        <div class="container app-content" id="app">
            @yield('content')
        </div>

        <div class="app-footer">
            @include('app.shared.layouts._footer')
        </div>
    </body>

    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token()]) !!};
    </script>

    <!-- JS -->
    <script src="{{ elixir('js/app.js') }}"></script>
</html>
