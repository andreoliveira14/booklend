@extends('app.shared.layouts.app')
@section('title', 'searching for ')

@section('content')

    {{-- Page Title --}}
    @include('app.shared.layouts._title', ['title' => 'Search'])

    <search-books :initial-books="{{ json_encode($books) }}"></search-books>

@endsection