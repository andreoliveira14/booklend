# Loan

* id
* price
* text
* bookId
* ownerId - owner of the book/loan
* duration - duration of the loans
------
* userId - user that will receive the book
* startDate - date of the exchange day
* endDate - date of the end of exchange day

* isCanceled - if user canceled the loan
* cancelDate - date of cancellation of the loan
* cancelText - reason of cancellation

* isFinished - if the loan was successfully completed
* finishDate - finishe date
* finishText - finish text

##### To post a loan
* User goes to shelf.
* User chooses one book and click on "create loan"
* User fills:
* @price - loan can be a price or is zero
* @text - some info text
* @duration - the duration the book is with another person
* And behind the scenes:
* @bookId - book chosen by the user
* @ownerId - owner userId that began the loan

##### To begin the loan
* Owner goes to the book/shelf and click in 'start loan with user'
* User fills:
* @userId - user that will have the book
* @startDate - start date
* @endDate - end date (apperas + duration)

##### To cancel the loan
* Owner goes to the book/shelf and click 'cancel'
* User fills:
* @cancelText - text saying why the loan was canceled
* And behind the scenes:
* @isCanceled - true
* @cancelDate - "today's" date

##### To finish the loan
* Owner goes to book/shelf (when he/she has the book returned) and click 'finish'
* User fills:
* @finishText - text
* @finishDate - finish date
* And behind the scenes:
* @isFinished - true
