/*
|
|--------------------------------------
| DEPENDENCIES
|--------------------------------------
|
*/

// Packages / Plugins / etc
window._ = require('lodash');
window.$ = window.jQuery = require('jquery');
window._ = require('jquery-form-validator');
//window._ = require('fine-uploader');
window.debounce = require('debounce');
window.swal = require('sweetalert2');

//vuejs
window.Vue = require('vue');
window.VeeValidate = require('vee-validate');
window.bus = new Vue();

Vue.use(VeeValidate);

require('bootstrap-sass');
require('vue-resource');

// Custom Files
require('./_formValidation');

/**
 * We'll register a HTTP interceptor to attach the "CSRF" header to each of
 * the outgoing requests issued by this application. The CSRF middleware
 * included with Laravel will automatically verify the header's value.
 */

Vue.http.interceptors.push((request, next) => {
    request.headers.set('X-CSRF-TOKEN', Laravel.csrfToken);

    // add date for the back button bug
    request.url += (request.url.indexOf('?') > 0 ? '&' : '?') + `cb=${new Date().getTime()}`;

    next();
});
