<?php

namespace App\Http\Controllers\App;

use App\Book;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| HomeController
|--------------------------------------------------------------------------
|
| This file has the action to show the homepage
|
*/
class FeedController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::with('user', 'images')->where('user_id', '<>', Auth::user()->id)->where('to_loan', true)->get();

        return view('app.feed.index', compact('books'));
    }
}
