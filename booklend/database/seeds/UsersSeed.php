<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Andre',
            'email' => 'andre@hotmail.com',
            'password' => bcrypt('123456'),
            'phone' => '07454789461',
            'address' => '51, Regent Road',
            'city' => 'Stockport',
            'country' => 'United Kingdom',
            'postal_code' => 'SK13RU',
        ]);

        User::create([
            'name' => 'Joao',
            'email' => 'joao@hotmail.com',
            'password' => bcrypt('123456'),
            'phone' => '07454789461',
            'address' => '51, Regent Road',
            'city' => 'Stockport',
            'country' => 'United Kingdom',
            'postal_code' => 'SK13RU',
        ]);
    }
}
