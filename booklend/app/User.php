<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'phone', 'address', 'city', 'country', 'postal_code', 'image', 'password',
    ];

    protected $appends = ['count_not_read_messages'];

    /**
     * The rules are difined at 'RegisterController'.
     *
     * @var array
     */
    protected $rules = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_admin' => 'boolean',
    ];

    /**
     * Books Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function books()
    {
        return $this->hasMany(Book::class);
    }

    /**
     * Books Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class, 'user_from_id');
    }

    /**
     * Books Relationship
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function getCountNotReadMessagesAttribute()
    {
        return Message::where('user_from_id', $this->id)->where('user_to_id', Auth::id())->where('was_read', false)->count();
    }
}
