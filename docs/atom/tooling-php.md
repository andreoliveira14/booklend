# Tooling to PHP to Atom

1. Install the Atom packages
1. Install the requirements of the Atom packages

## Atom Packages

Install this packages:

```bash
language-blade
autocomplete-php
linter-php
php-integrator-base
php-integrator-navigation
php-integrator-tooltips
php-integrator-annotations
php-integrator-refactoring
php-integrator-symbol-viewer
markdown-preview
```

#### On Windows
Copiar os ficheiros que estão neste repositório (ou então fazer download do [endereço](https://www.sqlite.org/2016/sqlite-dll-win64-x64-3130000.zip)
e colocá-los numa pasta que esteja no PATH, ou então criar uma pasta e colocá-la no PATH...
