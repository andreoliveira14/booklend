const elixir = require('laravel-elixir');

require('laravel-elixir-vue-2');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(mix => {
    mix.sass('app/app.scss')
       .webpack('app/app.js')

   /*
    |--------------------------------------------------------------------------
    | Copy certain resources to build folder for versioned files
    |--------------------------------------------------------------------------
    */
    .copy('./node_modules/font-awesome/fonts/**', 'public/assets/fonts/')
    .copy('./storage/images/**', 'public/assets/images/');
});
