<?php

namespace App\Http\Controllers\app;

use App\Book;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SearchController extends Controller
{
    public function index(Request $request) {
        $books = Book::paginate(40);

        return view('app.search.index', compact('books'));
    }
}
