<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Like extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'user_id', 'loan_id'
    ];

    /**
     * The rules to validate.
     *
     * @var array
     */
    protected $rules = [
        'user_id' => 'required',
        'loan_id' => 'required',
    ];
}
