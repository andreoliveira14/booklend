<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

/*
|--------------------------------------------------------------------------
| AUTHENTICATION ROUTES
|--------------------------------------------------------------------------
*/
Auth::routes();

/*
|--------------------------------------------------------------------------
| APP ROUTES
|--------------------------------------------------------------------------
*/

//Home

Route::group(['namespace' => 'App', 'middleware' => 'auth'], function () {

    // FEED
    Route::get('/', 'FeedController@index');

    // MESSAGE
    Route::get('/messages', 'MessagesController@index');
    Route::get('/messages/{user}', 'MessagesController@getMessages');
    Route::get('/messages/{user}/{book?}', 'MessagesController@create');
    Route::post('/messages/{user}/{book?}', 'MessagesController@store');

    // SEARCH
    Route::get('/search', 'SearchController@index');

    // USER
    Route::get('/users/{user}', 'UsersController@show');
    Route::get('/users/{user}/getBooks', 'UsersController@getBooks');

    // BOOKS
    Route::resource('/books', 'BooksController');
    Route::post('/books/{book}/toggleToLoan', 'BooksController@toggleToLoan');

    // BOOKS - IMAGE
    Route::post('/books/{book}/addImage', 'BookImagesController@store');
    Route::delete('/books/removeImage/{bookImage}', 'BookImagesController@destroy');

});

/*
|--------------------------------------------------------------------------
| ADMIN ROUTES
|--------------------------------------------------------------------------
*/
