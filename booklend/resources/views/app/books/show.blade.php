@extends('app.shared.layouts.app')
@section('title', 'Details - ' . $book->title)

@section('content')

    @if(Auth::id() !== $book->user_id)
        This is not my book, add filters
    @endif

    <div class="row">
        <div class="col-md-8 col-sm-6">

            <div class="row">
                <div class="col-md-4">
                    <img src="{{ $book->images->count() > 0 ? '/images/' . $book->images[0]->path : '/assets/images/book-def.jpg' }}">
                    <to-loan-button :id="{{ $book->id }}" :initial-to-loan="{{ $book->to_loan }}"></to-loan-button>
                </div>

                <div class="col-md-8">
                    <strong><h1> {{ $book->title }} </h1></strong>
                    <h5> by
                        <a href="/author">{{ $book->author }}</a>
                    </h5>
                    <p>
                        {{ $book->summary }}
                    </p>
                    <p><small><strong>Pages </strong></small> {{ $book->pages }}</p>
                    <p><small><strong>Published in </strong></small> {{ $book->year }}, by {{ $book->publisher }}</p>
                    <p><small><strong>ISBN </strong></small> {{ $book->isbn }}</p>
                    <p><small><strong>Language </strong></small> {{ $book->language->name }}</p>
                    <p><small><strong>Format </strong></small> {{ $book->format->name }}</p>
                    <p><small><strong>Genres </strong></small> {{ $book->genre->name }}</p>
                    <br>
                    <br>
                    <p><small><strong>Loan Duration</strong></small> {{ $book->loan_duration ? $book->loan_duration . ' days' : '-' }}</p>
                </div>
            </div>

        </div>
        <div class="col-md-4 col-sm-6">
            <show-book-main-actions :book="{{ $book }}"></show-book-main-actions>

            <hr>

            <p>
               <strong>
                   Current Loan
               </strong>
            </p>
            <p>
                Don't worry, this book is with you.
            </p>

        </div>
    </div>

    <hr>

    <div class="row">
        <div class="col-md-8">
            <p>
                <strong>
                    Last Loans
                </strong>
            </p>
            <p>
                You never lent this book.
            </p>
        </div>
    </div>

@endsection
