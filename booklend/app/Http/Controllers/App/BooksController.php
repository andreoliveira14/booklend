<?php

namespace App\Http\Controllers\App;

use App\BookImage;
use App\Http\Controllers\Controller;
use App\Http\Requests\BooksRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Language;
use App\Genre;
use App\Format;
use App\Book;
use Image;

/*
|
|------------------------------------------------------
| BooksController
|------------------------------------------------------
|
| BooksController has all the actions to interact with user books.
|
 */
class BooksController extends Controller
{
    /**
     * Entry point to see all the books.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()) {

            $books = Auth::user()->books()->with('images');

            if($request->has('search')) {
                $books->where('title', 'like' , '%' . $request->search . '%');
            }

            $books = $books->get();

            return compact('books');
        }

        return view('app.books.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if($request->ajax()) {
            $languages = Language::pluck('name', 'id')->toArray();
            $formats = Format::pluck('name', 'id')->toArray();
            $genres = Genre::pluck('name', 'id')->toArray();

            return compact('languages', 'formats', 'genres');
        }
    }

    /**
     * API - Store a newly created resource in storage.
     *
     * @param BooksRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(BooksRequest $request)
    {
        abort_unless(Auth::id(), 403);

        $book = new Book();
        $book = $book->fill($request->all());

        $book->user_id = Auth::id();

        $book->save();

        return compact('book');
    }

    /**
     * Display the specified resource.
     *
     * @param Book $book
     * @return \Illuminate\Http\Response
     * @internal param Book $id
     */
    public function show(Book $book)
    {
        $book->load('language', 'format', 'genre', 'images');

        return view('app.books.show', compact('book'));
    }

    /**
     * Toggle the 'to loan' attribute.
     *
     * @param Book $book
     * @return \Illuminate\Http\Response
     * @internal param Book $id
     */
    public function toggleToLoan(Book $book)
    {
        $book->to_loan = !$book->to_loan;
        $book->save();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BooksRequest $request
     * @param Book $book
     * @return \Illuminate\Http\Response
     * @internal param int $id
     */
    public function update(BooksRequest $request, Book $book)
    {
        $book->fill($request->all());
        $book->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Book $book
     * @return \Illuminate\Http\Response
     * @internal param Book $id
     */
    public function destroy(Book $book)
    {
        $book->delete();
    }
}
