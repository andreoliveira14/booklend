<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Loan extends Model
{
    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'price', 'text', 'duration', 'book_id', 'owner_id'
    ];

    /**
     * The rules to validate.
     *
     * @var array
     */
    protected $rules = [
        'book_id'     => 'required',
        'start_date'  => 'required|date_format:YYYY-MM-DD HH:MM:SS',
        'duration'   => 'required|numeric',
        'owner_id'    => 'required',
        'price'      => 'numeric',
        'end_date'    => 'date_format:YYYY-MM-DD HH:MM:SS',
    ];
}
