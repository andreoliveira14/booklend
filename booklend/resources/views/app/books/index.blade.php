@extends('app.shared.layouts.app')
@section('title', 'My Shelf')

@section('content')

    {{-- Page Title --}}
    @include('app.shared.layouts._title', ['title' => 'My Shelf (' . Auth::user()->books->count() . ')'])

    <books></books>

@endsection
