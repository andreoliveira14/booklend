<?php

namespace App\Http\Controllers\app;

use App\Book;
use App\Http\Controllers\Controller;
use App\Http\Requests\MessagesRequest;
use App\Message;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MessagesController extends Controller
{

    /**
     * Entry point to see all the messages.
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function index()
    {
        $messages = Message::with('user_from', 'user_to')->where('user_from_id', Auth::id())->orWhere('user_to_id', Auth::id())
            ->orderBy('created_at')->get();

        $users = collect();
        foreach($messages as $message) {
            if($message->user_to_id === Auth::id()) {
                $users->push($message->user_from);
            } else if($message->user_from_id === Auth::id()) {
                $users->push($message->user_to);
            }
        }

        // remove the auth user, get the unqiue values (use values, to reorganize the keys)
        $users = $users->diff(Auth::id())->unique()->values();

        $authUserId = Auth::id();

        return view('app.messages.index', compact('authUserId', 'users'));
    }

    /**
     * Entry point to get the messages between users.
     *
     * @param User $user
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function getMessages(User $user)
    {
        $messages = Message::with('user_from', 'user_to')->whereIn('user_from_id', [Auth::id(), $user->id])->whereIn('user_to_id', [Auth::id(), $user->id])
            ->orderBy('created_at')->get();

        // read the messages sent by the clicked user
        Message::where('user_from_id', $user->id)->where('user_to_id', Auth::id())
            ->orderBy('created_at')->where('was_read', false)->update(['was_read' => true]);

        return compact('messages');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param User $user
     * @param Book $book
     * @return \Illuminate\Http\Response
     * @internal param Request $request
     */
    public function create(User $user, Book $book)
    {
        $book->load('images');
        $user->load('books');

        return view('app.messages.send', compact('user', 'book'));
    }

    /**
     * API - Store a newly created resource in storage.
     *
     * @param MessagesRequest $request
     * @param User $user
     * @param Book $book
     * @return \Illuminate\Http\Response
     */
    public function store(MessagesRequest $request, User $user, Book $book)
    {
        $message = new Message();
        $message = $message->fill($request->all());

        $message->user_from_id = Auth::id();
        $message->user_to_id = $user->id;

        //if the message is associated with a book
        if(!empty($book->id)) {
            $message->book_id = $book->id;
        }

        $message->save();

        $message->load('user_from');

        return compact('message');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Message $message
     * @return \Illuminate\Http\Response
     * @internal param Book $book
     * @internal param Book $id
     */
    public function destroy(Message $message)
    {
        $message->delete();
    }

}
