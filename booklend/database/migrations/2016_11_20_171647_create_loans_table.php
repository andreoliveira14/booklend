<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create('loans', function (Blueprint $table) {
             $table->increments('id');
             $table->text('notes');
             //owner
             $table->integer('user_id')->unsigned();
             $table->foreign('user_id')->references('id')->on('users');
             // -----------
             $table->integer('user_id_loan')->unsigned();
             $table->foreign('user_id_loan')->references('id')->on('users');
             $table->dateTime('start_date');
             $table->dateTime('end_date');

             $table->boolean('is_canceled')->default(false);
             $table->dateTime('cancel_date');
             $table->text('cancel_text');
             
             $table->boolean('is_finished')->default(false);
             $table->dateTime('finish_date');
             $table->text('finish_text');
             $table->timestamps();
         });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::drop('loans');
     }
}
