/*
|
|----------------------------------------------------
| COMPONENTS FILES
|----------------------------------------------------
|
| This file has all the global components.
|
|
*/

/**
 * Global Components
 */
Vue.component('loading', require('vue-spinner/src/ClipLoader.vue'));
Vue.component('modal', require('../components/modal.vue'));
Vue.component('image-upload', require('../components/image-upload.vue'));

/**
 * Components
 */

/* Books */
Vue.component('books', require('./components/books/index.vue'));
Vue.component('to-loan-button', require('./components/books/to-loan-button.vue'));
Vue.component('show-book-main-actions', require('./components/books/show-book-main-actions.vue'));

/* Main Search */
Vue.component('search-books', require('./components/search/index.vue'));

/* Feed */
Vue.component('feed', require('./components/feed/index.vue'));

/* Messages */
Vue.component('messages', require('./components/messages/index.vue'));
Vue.component('send-message', require('./components/messages/send-message.vue'));

/* Profile / Users Details */
Vue.component('user-books', require('./components/users/user-books.vue'));
