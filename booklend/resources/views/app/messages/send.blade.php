@extends('app.shared.layouts.app')
@section('title', 'Send Message')

@section('content')

    {{-- Page Title --}}
    @include('app.shared.layouts._title', ['title' => 'Send Message'])

    <div class="row">
        <div class="col-md-5 col-sm-6">
            <send-message user-id="{{ $user->id }}" book-id="{{ empty($book->id) ? null : $book->id }}"></send-message>
        </div>
        @if(!empty($book->id))
            <div class="col-md-offset-1 col-md-6 col-sm-6">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4 app-underline-sub-title">
                                <strong>Sending message to </strong>
                            </div>
                        </div>

                        <p><small><strong>{{ $user->name }}</strong></small></p>

                        <img src="{{ $user->image ? 'images/' . $user->image : '/assets/images/user-def.jpg' }}" class="img-circle app-user-image-message"/>

                        <p>
                            <small><strong>Total Books:</strong></small> {{ $user->books->count() }}
                        </p>
                        <br>
                        <br>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-8 app-underline-sub-title">
                                <strong>Book details </strong>
                            </div>
                        </div>

                        @if($book->images->count() > 0)
                            <img src="{{'/images/' . $book->images[0]->path}}" />
                        @else
                            <img src="/assets/images/book-def.jpg" />
                        @endif
                    </div>
                    <div class="col-md-6">
                        <strong><h3> {{ $book->title }} </h3></strong>
                        <h5> by
                            <a href="/author">{{ $book->author }}</a>
                        </h5>
                        <p><small><strong>Pages </strong></small> {{ $book->pages }}</p>
                        <p><small><strong>Published in </strong></small> {{ $book->year }}, by {{ $book->publisher }}</p>
                        <p><small><strong>Language </strong></small> {{ $book->language->name }}</p>
                        <p><small><strong>Format </strong></small> {{ $book->format->name }}</p>
                        <p><small><strong>ISBN </strong></small> {{ $book->isbn }}</p>
                        <br>
                        <br>
                        <p><small><strong>Loan Duration</strong></small> {{ $book->loan_duration ? $book->loan_duration . ' days' : '-' }}</p>
                    </div>
                </div>
            </div>
        @endif
    </div>

@endsection