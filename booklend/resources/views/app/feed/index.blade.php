@extends('app.shared.layouts.app')

@section('content')
    <div class="row">
        <div class="col-md-12">

            <feed :books="{{ json_encode($books) }}"></feed>

        </div>
    </div>
@endsection
