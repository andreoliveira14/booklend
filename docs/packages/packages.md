# Composer
* Forms - laravelcollective (https://laravelcollective.com/docs/5.3/html#installation)
* Images - intervention/image (http://image.intervention.io/use/basics)

# npm
* Forms validation - formvalidator (http://www.formvalidator.net/index.html)

* Images (Frontend) - blueimp-file-upload (https://blueimp.github.io/jQuery-File-Upload/)
