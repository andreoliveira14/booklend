<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BooksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'       => 'required|min:2|max:50',
            'author'      => 'required|min:2|max:50',
            'summary'     => 'required|min:10|max:400',
            'pages'       => 'required|min:1',
            'isbn'        => 'required|digits_between:10,13',
            'year'        => 'required|numeric',
            'publisher'   => 'required|min:3|max:30',
            'language_id' => 'required',
            'format_id'   => 'required',
            'genre_id'    => 'required',
        ];
    }
}
